import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { CookieStorage, LocalStorage, SessionStorage } from 'ngx-store';

export interface Song {
  cover: string;
  name: string;
  artist: string;
}
@Component({
  selector: 'app-stepper',
  templateUrl: 'stepper.component.html',
  styleUrls: ['stepper.component.css'],
})
export class StepperComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  constructor(private _formBuilder: FormBuilder) {}
@Input() public listOfNames = [];
@Output()  msgEvent  = new EventEmitter();
@LocalStorage() filteredSongs: Observable<Song[]>;
@LocalStorage() songs: Song[];
nameModel: string;
names: string;
artist: string;
length: string;
youtube: string;
genre: string;
  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      fourthCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      fifthCtrl: ['', Validators.required]
    });
  }
  private _filterSongs(value: string): Song[] {
    const filterValue = value.toLowerCase();

    return this.songs.filter(song => song.name.toLowerCase().indexOf(filterValue) === 0);
  }
  onSubmit() {
    this.listOfNames.push(this.names  + ' - ' + this.artist  + ' - ' + this.length  + ' - ' + this.youtube + ' - ' + this.genre);
    this.nameModel = '';

  }

  onClear() {
    this.listOfNames = [];
  }
  emitChild() {
    this.msgEvent.emit([ {
      name: this.names,
      artist: this.artist,
      cover: 'https://upload.wikimedia.org/wikipedia/commons/3/35/Simple_Music.svg',
      youtube: this.youtube

    }]);
  }
}
