import { Component, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CookieStorage, LocalStorage, SessionStorage } from 'ngx-store';
import { trigger, state, style, animate, transition} from '@angular/animations';
export interface Song {
  cover: string;
  name: string;
  artist: string;
  youtube: any;
}
export interface Aura {
   duration: number;
   songName: string;
  }



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('popOverState', [
      state('showanimation', style({
        opacity: 1
      })),
      state('hideanimation',   style({
        opacity: 0
      })),
      transition('showanimation => hideanimation', animate('600ms ease-out')),
      transition('hideanimation => showanimation', animate('1000ms ease-in'))
    ])
  ]
})

export class AppComponent implements AfterViewInit {

  title = 'app';
  value = '';
  show = false;
  show2 = false;
  show3 = false;
  show4 = false;
  show5 = false;

  video = false;
  songCtrl = new FormControl();
  public filteredSongs: Song[];
  @LocalStorage()  public addedSongs: Song[] = [];
  public mysongs = [];
  showSelected = false;
  message = [];

  showanimation = false;


  @Output() songEvent = new EventEmitter();
  public songs: Song[] = [
    {
      name: 'Temporary Bliss',
      artist: 'The Cab',
      cover: 'https://upload.wikimedia.org/wikipedia/commons/3/35/Simple_Music.svg',
      youtube: 'https://www.youtube.com/embed/PnugraD0wOQ'
    },
    {
      name: 'That Man',
      artist: 'Caro Emerald',
      cover: 'https://upload.wikimedia.org/wikipedia/commons/3/35/Simple_Music.svg',
      youtube: 'https://www.youtube.com/embed/PnugraD0wOQ'

    },
    {
      name: 'Sweet Emotion',
      artist: 'The Kooks',
      cover: 'https://upload.wikimedia.org/wikipedia/commons/3/35/Simple_Music.svg',
      youtube: 'https://www.youtube.com/embed/PnugraD0wOQ'

    },
    {
      name: 'Miracle',
      artist: 'The Score',
      cover: 'https://upload.wikimedia.org/wikipedia/commons/3/35/Simple_Music.svg',
      youtube: 'https://www.youtube.com/watch?v=PnugraD0wOQ'

    }
  ];
  public showAura: Aura[] = [
    { duration: 3, songName: 'September Song'},
    { duration: 4, songName: 'Naive'},
    { duration: 3, songName: 'Revolution'},
    { duration: 5, songName: 'Legend'},
    { duration: 4, songName: 'Just My Type'}
   ];

   get stateName() {
    return this.showanimation ? 'showanimation' : 'hideanimation';
  }

  toggle () {
    this.showanimation = !this.showanimation;
  }
  constructor() {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.songCtrl.valueChanges
        .pipe(
          startWith(''),
          map(song => song ? this._filterSongs(song) : this.songs.slice())
        ).subscribe(songs => {
          this.filteredSongs = songs;
          if (this.filteredSongs.length === this.songs.length) {
            this.addedSongs.forEach((s) => {
              this.filteredSongs.push(s);
            });
          }
        });
    });

  }

  private _filterSongs(value: string): Song[] {
    const filterValue = value.toLowerCase();

    return this.songs.filter(song => (song.name.toLowerCase().indexOf(filterValue) === 0) ||
    (song.artist.toLowerCase().indexOf(filterValue) === 0)) ;

  }

  public receiveValue($event) {
    this.addedSongs.push($event[0]);
  }


  add() {
    this.showSelected = true;
    alert('Do you want to add  ' + this.value + '?');
    this.message[0] = this.value;

  }

  receiveSong($event) {
this.value = $event;

  }

}
