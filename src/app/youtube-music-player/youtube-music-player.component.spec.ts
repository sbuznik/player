import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubeMusicPlayerComponent } from './youtube-music-player.component';

describe('YoutubeMusicPlayerComponent', () => {
  let component: YoutubeMusicPlayerComponent;
  let fixture: ComponentFixture<YoutubeMusicPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoutubeMusicPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeMusicPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
