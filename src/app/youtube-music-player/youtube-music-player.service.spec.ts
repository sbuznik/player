import { TestBed, inject } from '@angular/core/testing';

import { YoutubeMusicPlayerService } from './youtube-music-player.service';

describe('YoutubeMusicPlayerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YoutubeMusicPlayerService]
    });
  });

  it('should be created', inject([YoutubeMusicPlayerService], (service: YoutubeMusicPlayerService) => {
    expect(service).toBeTruthy();
  }));
});
