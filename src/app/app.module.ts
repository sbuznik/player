import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatAutocompleteModule, MatInputModule, MatMenu,  } from '@angular/material';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import {MatFormFieldModule, MatFormField} from '@angular/material/form-field';
import {MatToolbarModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import { StepperComponent } from './stepper/stepper.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatMenuModule} from '@angular/material/menu';
import { PlaylistsComponent } from './playlists/playlists.component';
import {AudioPlayerNgModule} from 'audio-player-ng';
import {HttpClientModule} from '@angular/common/http';
import { Player2Component } from './player2/player2.component';
import { YoutubeMusicPlayerModule } from 'youtube-music-player';
import { YoutubeMusicPlayerComponent } from './youtube-music-player/youtube-music-player.component';
import { YoutubeMusicPlayerService } from './youtube-music-player/youtube-music-player.service';
import {MatSelectModule} from '@angular/material/select';
import { AuraShowComponent } from './aura-show/aura-show.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MyplaylistComponent } from './myplaylist/myplaylist.component';
import { WebStorageModule } from 'ngx-store';
import {MatListModule} from '@angular/material/list';
import { PhotoGalleryComponent } from './photo-gallery/photo-gallery.component';

@NgModule({
  declarations: [
    AppComponent,
 StepperComponent,
 PlaylistsComponent,
 Player2Component,
 YoutubeMusicPlayerComponent,
 AuraShowComponent,
 MyplaylistComponent,
 PhotoGalleryComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    BrowserAnimationsModule,
    HttpModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
MatToolbarModule,
MatButtonModule,
MatStepperModule,
MatMenuModule,
HttpClientModule,
AudioPlayerNgModule,
YoutubeMusicPlayerModule,
MatSelectModule,
MatTooltipModule,
WebStorageModule,
MatListModule
  ],
  exports: [ MatFormField, MatToolbarModule, MatButtonModule
    , MatStepperModule,  ],
  providers: [YoutubeMusicPlayerService],
  bootstrap: [AppComponent]
})


export class AppModule { }
