import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CookieStorage, LocalStorage, SessionStorage } from 'ngx-store';

@Component({
  selector: 'app-myplaylist',
  templateUrl: './myplaylist.component.html',
  styleUrls: ['./myplaylist.component.css']
})

export class MyplaylistComponent {

  @LocalStorage() selectedValue: string;
  selectedValue1: string;
  selectedDuration: number;
  selectedValue2: string;
  selectedDuration2: number;
  sum: number;
  video = false;
  video2 = false;
  video3 = false;
  video4 = false;
  video5 = false;
  youtubePlay: number;
  songs = new FormControl();

  public songList: any[] = ['Temporary Bliss',
    'That Man',
    'Sweet Emotion',
    'Miracle'
  ];
}
