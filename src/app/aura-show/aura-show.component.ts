import { Component, OnInit, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-aura-show',
  templateUrl: './aura-show.component.html',
  styleUrls: ['./aura-show.component.css']
})
export class AuraShowComponent {
  selectedValue: string;

  selectedValue1: string;
  selectedDuration: number;
  selectedValue2: string;
  selectedDuration2: number;
  sum: number;

  constructor(private zone: NgZone) {

  }

  songs = new FormControl();
  songList: string[] = ['September Song - 3.5', 'Legend - 3', 'Naive - 3.5 ',
    'Revolution - 4.0', 'She Moves In Her Own Way - 4.5', 'Just My Type - 4.5 '];

  public run() {
    this.sum = (this.selectedDuration * 1) + (this.selectedDuration2 * 1);
  }
}
