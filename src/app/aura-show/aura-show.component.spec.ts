import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuraShowComponent } from './aura-show.component';

describe('AuraShowComponent', () => {
  let component: AuraShowComponent;
  let fixture: ComponentFixture<AuraShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuraShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuraShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
